# Hailstorm Engine
A basic game engine written in C++. This engine was designed to accomodate multiple rendering methods (software, opengl, directx) and targets (windows, linux, android).

# Dependencies
This project depends on Qt currently and other dependencies will possibly be added (ie. OpenGL and DirectX libraries). However, once dependencies become bulky enough the plan is to be able to disable them through the CMake file.

