#ifndef _HAILSTORM_QT_GAME_WINDOW_H_
#define _HAILSTORM_QT_GAME_WINDOW_H_

#include <QMainWindow>

#include "renderer.h"

namespace hailstorm::gfx {
class QtGameWindow : public QMainWindow {
    Q_OBJECT

   public:
    QtGameWindow(Renderer *renderer, QWidget *parent);
    ~QtGameWindow();

    void paintEvent(QPaintEvent *event) override;
    void closeEvent(QCloseEvent *event) override;

    void refresh();

    bool isRunning() { return m_running; }

   private:
    Renderer *m_renderer;
    bool m_running;
};
};  // namespace hailstorm::gfx

#endif