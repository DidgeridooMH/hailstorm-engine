#ifndef _HAILSTORM_SOFTWARE_RENDERER_H_
#define _HAILSTORM_SOFTWARE_RENDERER_H_

#include "renderer.h"

namespace hailstorm::gfx {
class SoftwareRenderer : public Renderer {
   public:
    SoftwareRenderer(uint32_t width, uint32_t height);
    ~SoftwareRenderer();

    const uint32_t *getPixelBuffer() override { return m_pixel_buffer; };

    void draw(float x, float y, float z, uint32_t color) override;
    void drawLine(const Point &p1, const Point &p2) override;
    void drawTriangle(const struct Triangle &tri) override;
    void drawFilledTriangle(const struct Triangle &tri) override;
    void drawMesh(const struct Mesh &mesh, float deltaTime) override;

    void clearScreen(uint32_t color) override;

    const uint32_t getWidth() override { return m_width; }
    const uint32_t getHeight() override { return m_height; }

   private:
    uint32_t m_width, m_height;
    uint32_t *m_pixel_buffer;
    float theta = 0;
};
}  // namespace hailstorm::gfx

#endif