#include <QApplication>
#include <QCloseEvent>
#include <QPainter>
#include <QStatusBar>
#include <chrono>
#include <cmath>
#include <sstream>

#include "qtgamewindow.h"

using namespace std::chrono;

namespace hailstorm::gfx {
QtGameWindow::QtGameWindow(Renderer *renderer, QWidget *parent)
    : QMainWindow(parent), m_renderer(renderer), m_running(true) {
    setFixedSize(renderer->getWidth(), renderer->getHeight());
    show();
}

QtGameWindow::~QtGameWindow() {
    // Default deconstructor
}

void QtGameWindow::paintEvent(QPaintEvent *event) {
    QPainter painter(this);
    QImage image((uint8_t *)m_renderer->getPixelBuffer(),
                 m_renderer->getWidth(), m_renderer->getHeight(),
                 QImage::Format_ARGB32);

    painter.fillRect(rect(), Qt::black);
    painter.drawPixmap(0, 0, QPixmap::fromImage(image));
}

void QtGameWindow::closeEvent(QCloseEvent *event) {
    event->accept();
    m_running = false;
}

void QtGameWindow::refresh() {
    static auto start = system_clock::now();
    auto end = system_clock::now();

    float frame_time = duration_cast<milliseconds>(end - start).count();
    std::stringstream fps;
    fps.precision(2);
    fps << "Hailstorm - " << std::fixed << (1000.0f / frame_time) << " fps";
    setWindowTitle(QString::fromStdString(fps.str()));

    QApplication::processEvents();
    update();
    start = end;
}
};  // namespace hailstorm::gfx
