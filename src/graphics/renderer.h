#ifndef _HAILSTORM_RENDERER_H_
#define _HAILSTORM_RENDERER_H_

#include "../common/vecmath.h"
#include "common/color.h"

namespace hailstorm::gfx {
class Renderer {
   public:
    virtual void draw(float x, float y, float z, uint32_t color) = 0;
    virtual void drawLine(const Point &p1, const Point &p2) = 0;
    virtual void drawTriangle(const struct Triangle &tri) = 0;
    virtual void drawFilledTriangle(const struct Triangle &tri) = 0;
    virtual void drawMesh(const struct Mesh &mesh, float deltaTime) = 0;

    virtual void clearScreen(uint32_t color) = 0;

    virtual const uint32_t *getPixelBuffer() = 0;

    virtual const uint32_t getWidth() = 0;
    virtual const uint32_t getHeight() = 0;
};
}  // namespace hailstorm::gfx

#endif