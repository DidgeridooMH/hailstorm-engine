#include <QApplication>
#include <QCloseEvent>
#include <QPainter>
#include <QStatusBar>
#include <cmath>
#include <iostream>

#include "softwarerenderer.h"

namespace hailstorm::gfx {
SoftwareRenderer::SoftwareRenderer(uint32_t width, uint32_t height)
    : m_width(width), m_height(height) {
    m_pixel_buffer = new uint32_t[width * height];
    memset(m_pixel_buffer, 0, width * height * sizeof(uint32_t));
}

SoftwareRenderer::~SoftwareRenderer() { delete[] m_pixel_buffer; }

void SoftwareRenderer::clearScreen(uint32_t color) {
    memset(m_pixel_buffer, 0, m_width * m_height * sizeof(uint32_t));
}

void SoftwareRenderer::draw(float x, float y, float z, uint32_t color) {
    if (x < m_width && y < m_height && x >= 0 && y >= 0 && z > 0) {
        m_pixel_buffer[(int)x + (int)y * m_width] = color;
    }
}

void SoftwareRenderer::drawLine(const Point &p1, const Point &p2) {
    Point pt1 = p1, pt2 = p2;
    pt1.x = (p1.x + 1.0f) * m_width / 2;
    pt2.x = (p2.x + 1.0f) * m_width / 2;
    pt1.y = (p1.y + 1.0f) * m_height / 2;
    pt2.y = (p2.y + 1.0f) * m_height / 2;

    if ((pt1.x > m_width || pt1.y > m_height || pt1.x < 0 || pt1.y < 0 ||
         pt1.z < 0) &&
        (pt2.x > m_width || pt2.y > m_height || pt2.x < 0 || pt2.y < 0 ||
         pt2.z < 0)) {
        return;
    }

    float dx = pt2.x - pt1.x;
    float dy = pt2.y - pt1.y;
    if (dx == 0.0f && dy == 0.0f) {
        draw(pt1.x, pt1.y, 1.0f, pt1.color.toU32Int());
        return;
    }

    if (fabs(dx) > fabs(dy)) {
        float xmin, xmax;

        if (pt1.x < pt2.x) {
            xmin = pt1.x;
            xmax = pt2.x;
        } else {
            xmin = pt2.x;
            xmax = pt1.x;
        }

        float slope = dy / dx;
        for (float x = xmin; x <= xmax; x += 1.0f) {
            float y = pt1.y + ((x - pt1.x) * slope);
            draw(x, y, 1.0f,
                 (pt1.color + ((pt2.color - pt1.color) * ((x - pt1.x) / dx)))
                     .toU32Int());
        }
    } else {
        float ymin, ymax;
        if (pt1.y < pt2.y) {
            ymin = pt1.y;
            ymax = pt2.y;
        } else {
            ymin = pt2.y;
            ymax = pt1.y;
        }

        float slope = dx / dy;
        for (float y = ymin; y <= ymax; y += 1.0f) {
            float x = pt1.x + ((y - pt1.y) * slope);
            draw(x, y, 1.0f,
                 (pt1.color + ((pt2.color - pt1.color) * ((y - pt1.y) / dy)))
                     .toU32Int());
        }
    }
}

void SoftwareRenderer::drawTriangle(const struct Triangle &tri) {
    drawLine(tri.pts[0], tri.pts[1]);
    drawLine(tri.pts[1], tri.pts[2]);
    drawLine(tri.pts[2], tri.pts[0]);
}

void SoftwareRenderer::drawFilledTriangle(const struct Triangle &tri) {
    Vec2 v1 = {(tri.pts[0].x + 1.0f) * m_width / 2,
               (tri.pts[0].y + 1.0f) * m_height / 2};
    Vec2 v2 = {(tri.pts[1].x + 1.0f) * m_width / 2,
               (tri.pts[1].y + 1.0f) * m_height / 2};
    Vec2 v3 = {(tri.pts[2].x + 1.0f) * m_width / 2,
               (tri.pts[2].y + 1.0f) * m_height / 2};

    if ((v1.x > m_width || v1.y > m_height || v1.x < 0 || v1.y < 0 ||
         tri.pts[0].z < 0) &&
        (v2.x > m_width || v2.y > m_height || v2.x < 0 || v2.y < 0 ||
         tri.pts[1].z < 0) &&
        (v3.x > m_width || v3.y > m_height || v3.x < 0 || v3.y < 0 ||
         tri.pts[2].z < 0)) {
        return;
    }

    /* get the bounding box of the triangle */
    int maxX = std::max(v1.x, std::max(v2.x, v3.x));
    int minX = std::min(v1.x, std::min(v2.x, v3.x));
    int maxY = std::max(v1.y, std::max(v2.y, v3.y));
    int minY = std::min(v1.y, std::min(v2.y, v3.y));

    /* spanning vectors of edge (v1,v2) and (v1,v3) */
    Vec2 vs1 = {v2.x - v1.x, v2.y - v1.y};
    Vec2 vs2 = {v3.x - v1.x, v3.y - v1.y};
    float dy = v2.y - v1.y;

    for (int x = minX; x <= maxX; x++) {
        for (int y = minY; y <= maxY; y++) {
            Vec2 q = {x - v1.x, y - v1.y};

            float s = (float)crossProduct(q, vs2) / crossProduct(vs1, vs2);
            float t = (float)crossProduct(vs1, q) / crossProduct(vs1, vs2);

            if ((s >= 0) && (t >= 0) && (s + t <= 1)) {
                /* inside triangle */
                draw(x, y, 1.0f, 0xFF00FFFF);
            }
        }
    }
}

void SoftwareRenderer::drawMesh(const struct Mesh &mesh, float deltaTime) {
    theta += 0.001f * deltaTime;

    float near = 0.1f;
    float far = 1000.0f;
    float fov = 90.0f;
    float aspect = (float)m_height / m_width;
    float fovRad = 1.0f / tanf(fov * 0.5f / 180.0f * 3.14159f);

    Mat4x4 matProj;
    matProj.m[0][0] = aspect * fovRad;
    matProj.m[1][1] = fovRad;
    matProj.m[2][2] = far / (far - near);
    matProj.m[3][2] = (-far * near) / (far - near);
    matProj.m[2][3] = 1.0f;
    matProj.m[3][3] = 0.0f;

    Mat4x4 matRotZ, matRotX;

    // Rotation Z
    matRotZ.m[0][0] = cosf(theta);
    matRotZ.m[0][1] = sinf(theta);
    matRotZ.m[1][0] = -sinf(theta);
    matRotZ.m[1][1] = cosf(theta);
    matRotZ.m[2][2] = 1;
    matRotZ.m[3][3] = 1;

    // Rotation X
    matRotX.m[0][0] = 1;
    matRotX.m[1][1] = cosf(theta * 0.5f);
    matRotX.m[1][2] = sinf(theta * 0.5f);
    matRotX.m[2][1] = -sinf(theta * 0.5f);
    matRotX.m[2][2] = cosf(theta * 0.5f);
    matRotX.m[3][3] = 1;

    for (auto tri : mesh.tris) {
        Triangle triProj, triRotatedZ, triRotatedX, triTranslated;

        matrixMultiply(tri.pts[0], triRotatedZ.pts[0], matRotZ);
        matrixMultiply(tri.pts[1], triRotatedZ.pts[1], matRotZ);
        matrixMultiply(tri.pts[2], triRotatedZ.pts[2], matRotZ);

        matrixMultiply(triRotatedZ.pts[0], triRotatedX.pts[0], matRotX);
        matrixMultiply(triRotatedZ.pts[1], triRotatedX.pts[1], matRotX);
        matrixMultiply(triRotatedZ.pts[2], triRotatedX.pts[2], matRotX);

        triTranslated = triRotatedX;
        triTranslated.pts[0].z =
            triRotatedX.pts[0].z + 10.0f * fabs(sinf(theta)) + 4.0f;
        triTranslated.pts[1].z =
            triRotatedX.pts[1].z + 10.0f * fabs(sinf(theta)) + 4.0f;
        triTranslated.pts[2].z =
            triRotatedX.pts[2].z + 10.0f * fabs(sinf(theta)) + 4.0f;

        // Project triangles from 3D --> 2D
        matrixMultiply(triTranslated.pts[0], triProj.pts[0], matProj);
        matrixMultiply(triTranslated.pts[1], triProj.pts[1], matProj);
        matrixMultiply(triTranslated.pts[2], triProj.pts[2], matProj);

        drawFilledTriangle(triProj);
        drawTriangle(triProj);
    }
}
};  // namespace hailstorm::gfx