#include "color.h"

Color::Color() {
    a = 0;
    r = 0;
    g = 0;
    b = 0;
}

Color::Color(uint32_t argb) {
    a = ((argb >> 24) & 0xFF) / 255.0f;
    r = ((argb >> 16) & 0xFF) / 255.0f;
    g = ((argb >> 8) & 0xFF) / 255.0f;
    b = ((argb >> 0) & 0xFF) / 255.0f;
}

Color::Color(float r, float g, float b, float a) : r(r), g(g), b(b), a(a) {}

uint32_t Color::toU32Int() const {
    uint32_t a32 = (int)(a * 255.0f) & 0xFF;
    uint32_t r32 = (int)(r * 255.0f) & 0xFF;
    uint32_t g32 = (int)(g * 255.0f) & 0xFF;
    uint32_t b32 = (int)(b * 255.0f) & 0xFF;

    return (a32 << 24) | (r32 << 16) | (g32 << 8) | (b32);
}