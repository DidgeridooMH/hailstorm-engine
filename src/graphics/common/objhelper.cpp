#include <fstream>
#include <iterator>
#include <sstream>
#include <string>

#include "objhelper.h"

struct Face {
    int a, b, c;
};

Mesh* getMeshFromObj(std::string path) {
    std::ifstream objfile(path);

    std::vector<Point> vertices;
    std::vector<Face> faces;

    uint32_t colors[] = {0xFF0000FF, 0xFF00FFFF, 0xFFFF0000, 0xFF00FF00};
    int c = 0;

    std::string line;
    while (std::getline(objfile, line)) {
        std::istringstream iss(line);
        std::istream_iterator<std::string> split = (iss);
        if (*split == std::string("v")) {
            split++;
            float x, y, z;
            x = std::stof(*split++);
            y = std::stof(*split++);
            z = std::stof(*split++) + 5.0f;
            vertices.push_back({x, y, z, colors[c]});
            c = (c + 1) % 4;
        } else if (*split == std::string("f")) {
            split++;
            int a, b, c;
            a = std::stoi(*split++) - 1;
            b = std::stoi(*split++) - 1;
            c = std::stoi(*split++) - 1;
            faces.push_back({a, b, c});
        }
    }

    Mesh* obj = new Mesh();
    for (auto f : faces) {
        obj->tris.push_back({vertices[f.c], vertices[f.b], vertices[f.a]});
    }

    return obj;
}
