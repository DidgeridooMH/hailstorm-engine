#ifndef OBJ_HELPER_H
#define OBJ_HELPER_H

#include "../../common/vecmath.h"

Mesh *getMeshFromObj(std::string path);

#endif
