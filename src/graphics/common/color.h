#ifndef COLOR_H
#define COLOR_H

#include <cstdint>
#include <iostream>

class Color {
   public:
    Color();
    Color(uint32_t argb);
    Color(float r, float g, float b, float a);

    friend Color operator+(const Color &c1, const Color &c2) {
        return Color(c1.r + c2.r, c1.g + c2.g, c1.b + c2.b, 1.0f);
    }

    friend Color operator-(const Color &c1, const Color &c2) {
        return Color(c1.r - c2.r, c1.g - c2.g, c1.b - c2.b, 1.0f);
    }

    friend Color operator*(const Color &c, float f) {
        return Color(c.r * f, c.g * f, c.b * f, c.a);
    }

    uint32_t toU32Int() const;

    float r, g, b, a;
};

#endif
