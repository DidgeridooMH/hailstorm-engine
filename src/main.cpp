#include <QApplication>
#include <QPainter>
#include <QWidget>
#include <chrono>
#include <cmath>
#include <iostream>
#include <sstream>
#include <string>
#include <thread>

#include "graphics/common/objhelper.h"
#include "graphics/qtgamewindow.h"
#include "graphics/softwarerenderer.h"

using ms = std::chrono::duration<float, std::milli>;

int main(int argc, char *argv[]) {
    QApplication app(argc, argv);

    hailstorm::gfx::SoftwareRenderer *renderer =
        new hailstorm::gfx::SoftwareRenderer(800, 600);
    hailstorm::gfx::QtGameWindow *window =
        new hailstorm::gfx::QtGameWindow(renderer, nullptr);

    Mesh *mesh = getMeshFromObj(std::string(argv[1]));

    auto start = std::chrono::system_clock::now();
    while (window->isRunning()) {
        auto end = std::chrono::system_clock::now();
        float dt = std::chrono::duration_cast<ms>(end - start).count();

        start = std::chrono::system_clock::now();

        renderer->clearScreen(0);
        renderer->drawMesh(*mesh, dt);

        window->refresh();
    }

    delete mesh;
    delete window;

    return 0;
}
