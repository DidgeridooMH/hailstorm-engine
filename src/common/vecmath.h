#ifndef VECMATH_H
#define VECMATH_H

#include <vector>

#include "../graphics/common/color.h"

struct Vec2 {
    float x;
    float y;
};

struct Point {
    float x;
    float y;
    float z;
    Color color;
};

struct Triangle {
    Point pts[3];
};

struct Mesh {
    std::vector<Triangle> tris;
};

struct Mat4x4 {
    float m[4][4] = {0};
};

float crossProduct(Vec2 v1, Vec2 v2);

void matrixMultiply(Point &i, Point &o, Mat4x4 &m);

#endif